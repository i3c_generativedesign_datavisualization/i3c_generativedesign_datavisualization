//********************************************
//
//              ×× DATA ××
//
//********************************************

PShape s;

//********************************************
//
//              ×× SETUP ××
//
//********************************************
void setup() {

  size(800, 800);
  background(255);
  smooth();
  noStroke();
  //  ------  you can choose between 'utensils', 
  //  ------  'cross', 'hammer' ou 'lotsofcircles'
  s = loadShape("cross.svg");
  //  ------  Disables the shape's style data and uses
  //  ------  Processing's current styles. Styles include
  //  ------  attributes such as colors, stroke weight, 
  //  ------  and stroke joints.
  s.disableStyle();

  //  ------  the radian of the circle
  float radian   = width/4;
  float count    = 0.0;
  float angle    = 360f / 800f; 

  for (int i = 0; i < width; i += 5) {
    //  ------  pick coordinates with trigonometry method
    float x      = width/2 + cos(count) * radian;
    float y      = height/2 + sin(count) * radian;

    float taille = random(8, 100);
    color c      = color(random(220, 250), random(150, 200), random(80));

    pushMatrix();
    translate(x, y);
    rotate(count +  7*(PI/4));

    fill(c);
    shape(s, 0, 0, taille, taille);

    popMatrix();

    count += angle;
  }
}
//********************************************
//
//              ×× DRAW ××
//
//********************************************

void draw() {
}

//********************************************
//
//            ×× KEYPRESSED ××
//
//********************************************

void keyPressed() {
  if (key == 's' || key == 'S') {
    saveFrame("pic"+timestamp()+"_##.png");
  }
}
//********************************************
//
//              ×× TIMESTAMP ××
//
//********************************************

String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

