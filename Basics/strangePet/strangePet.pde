/*
	< StrangePet >
	Copyright (C) 2012 BERNARDI Thomas.
	
	this program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the license, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public license for more details.	
	
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


//  ------  Only 2 variables for all the different points
//  ------  because there are defined into the double loop
float nX = 0.0;
float nY = 0.0;

void setup() {
  size(300, 300);
  smooth();
}

void draw() {
  background(255);
  
  
  for (int i=0; i<=width; i+=30) {
    for (int j=height; j>=0; j-=30) {
      
      //  ------  for each time it goes through the loop
      //  ------  the nX and nY variables are changing
      nX       += 0.00012;
      nY       += 0.00017;
      float _nX = noise(nX) * width;
      float _nY = noise(nY) * height;

      stroke(0, 100);     
      line(i, j, _nX, _nY);
      noStroke();
      ellipse(_nX, _nY, 30, 30);
    }
  }
}

