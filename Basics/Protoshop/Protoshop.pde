// the color you can choose with mouse pointer
color pick;


void setup() {
  //  ------  I paint the background in white, only on time at the start.
  background(255);
  size(600, 300);
  //  ------  a new size for the stroke
  strokeWeight(5);
  //  ------  add an anti-aliazing
  // smooth();
}

void draw() {

  //  ------  first a draw my palette
  noStroke();  

  for (int i=0; i<width/2; i+=5) {
    for (int j=0; j<height; j+=5) {
      float R = j*1.4;
      float G = i*0.8;
      float B = j*0.5; 
      color c = color(R, G, B);
      fill(c);
      //  ------  each square of my palette is drawing here
      rect(i, j, 5, 5);
    }
  }
  //  ------  this is to show us, the color I have
  //  ------  pick during my painting
  fill(pick);
  rect(0, 0, 300, 10);
  //  ------  choose a color with your mouse pointer
  if (mousePressed) {
    if (mouseX < width/2) {
      //  ------  this method give me the red, 
      //  ------  green and blue values at my mouse pointer
      pick = get(mouseX, mouseY);
    }
    //  ------  and now I can paint with this color
    if (mouseX > width/2) {
      stroke(pick);
      point(mouseX, mouseY);
    }
  }
}

void keyPressed() {
  if (key == 's') {
    PImage saveIt = get(width/2, 0, width/2, height);
    saveIt.save("myBeautifulDrawing.png");
  }
  if (key == 'e') {
    fill(255);
    rect(width/2, 0, width/2, height);
  }
}

