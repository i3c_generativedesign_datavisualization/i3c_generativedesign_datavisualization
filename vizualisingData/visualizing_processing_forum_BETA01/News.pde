
class News {

//********************************************
//
//              ×× DATA ××
//
//********************************************

  float xPos, yPos;                // ------ coordinates for each topic's bubble 
  float x_TopicL, y_TopicL;        // ------ coordinates for each topic's name, on the left side of the screen
  float x_TopicR, y_TopicR;        // ------ coordinates for each topic's name, on the right side of the screen
  float zoom;                      // ------ scaling bubble's size when I'm over it.
  float sizeOfTopic;               // ------ size of my topic's bubble depending on the number of replies  
  float w_Topic, h_Topic;          // ------ size of my topic's text
  float x_speed, y_speed;          // ------ speed of my topic's bubble
  float rot = 0.0;                 // ------ rotation of the line in the center of the topic's bubble, when I already seen the content.
  int counter = 0;                 // ------ We use this variable to limit the number of tabs opened with firefox
  int timer = 0;                   // ------ used to valid the alreadyseen boolean variable
  int x_direction, y_direction;    // ------ direction of my topic's bubble.
  int direction[] = {
    -1, 1
  };

  String content;                  // ------ title of the topic
  String dateOfTheNews;            // ------ date of the post (we don't use this for now)
  String urlOfTheNews;             // ------ url to the topic
  String urlOfTheNews_forSearching;// ------ url version for the search into the HTML page 
  String authorOfTheNews;          // ------ author of the topic (we don't use this for now);
  String categoryOfTheNews;        // ------ category of the topic, we use this for the color sorting.

  color shapeColor;
  color shapeOverColor;
  color shapeTempColor;
  color textColor;
  color shapeClickColor;
  color topicColor;


  boolean printTopic = false;
  boolean alreadySeen = false;
  
//********************************************
//
//            ×× CONSTRUCTOR ××
//
//********************************************

  News(float _xPos, float _yPos, String _content, String _dateOfTheNews, String _urlOfTheNews, String _authorOfTheNews, String _categoryOfTheNews, color _shapeColor) {

    xPos                      = _xPos;
    yPos                      = _yPos;
    x_speed                   = random(0.1, 0.8);
    y_speed                   = random(0.1, 0.8);
    x_direction               = direction[(int)random(2)];
    y_direction               = direction[(int)random(2)];
    sizeOfTopic               = 20;                          //------ size for a topic without any replies
    content                   = _content;
    dateOfTheNews             = _dateOfTheNews;
    urlOfTheNews              = _urlOfTheNews;

    zoom                      = 1;
    w_Topic                   = textWidth(content);
    h_Topic                   = textAscent() + textDescent();

    shapeColor                = _shapeColor;
    shapeTempColor            = shapeColor;
    shapeOverColor            = color(red(shapeColor)-20, green(shapeColor)-20, blue(shapeColor)-20, alpha(shapeColor));
    shapeClickColor           = color(red(shapeColor)-40, green(shapeColor)-40, blue(shapeColor)-40, alpha(shapeColor));
    textColor                 = color(0, 0, 125, 175);
    topicColor                = color(#ADC9D3, 75);

    searchingReplies();
  }
  
//********************************************
//
//            ×× SEARCHINGREPLIES ××
//
//********************************************

  void searchingReplies() {
    String temp[] = null;
    //------ we remove "http" form the String because in the HTML file, te url use "https" :-/
    temp = splitTokens(urlOfTheNews, ":#");

    urlOfTheNews_forSearching = "href=\"https:"+temp[1]+"\">";      //------ we add some letters to our URL for searching into HTML page. use \" to add quotation mark into your String

    String [] sentences = split(HTMLpage, " ");

    for (int i=0; i<sentences.length; i++) {
      if (sentences[i].equals("Replies")) {
        if (sentences[i-2].equals(urlOfTheNews_forSearching)) {
          sizeOfTopic += Integer.parseInt(sentences[i-1]) * 5;
          //println("le nombre de réponses pour le topic "+urlOfTheNews_forSearching+" est de : "+ sentences[i-1]);
        }
      }
    }
  }

//********************************************
//
//            ×× UPDATE ××
//
//********************************************

  void update() {

    x_TopicL = mouseX + sizeOfTopic/2;
    y_TopicL = mouseY - sizeOfTopic/2;
    x_TopicR = mouseX - w_Topic;
    y_TopicR = y_TopicL;


    if (overCircle(this.xPos, this.yPos, this.sizeOfTopic)) {
      shapeTempColor    = shapeOverColor;
      zoom              = 2.2;
      printTopic        = true;
      x_speed = y_speed = 0;
      if (timer > 60) {
        alreadySeen       = true;
      }
      timer ++;
     
      
      if (mousePressed && counter == 0) {
        shapeTempColor  = shapeClickColor;
        link(urlOfTheNews);
        counter++;
      }
    }
    else if (!overCircle(this.xPos, this.yPos, this.sizeOfTopic)) {
      shapeTempColor    = shapeColor;
      zoom              = 1;
      counter           = 0;
      timer             = 0;
      printTopic        = false;
      x_speed           = random(0.1, 0.8);
      y_speed           = random(0.1, 0.8);
    }
  }

//********************************************
//
//              ×× MOVE ××
//
//********************************************

  void move() {

    xPos = xPos + (x_speed * x_direction);
    yPos = yPos + (y_speed * y_direction);

    if (xPos > width - sizeOfTopic/2 || xPos < 0 + sizeOfTopic/2) {
      x_direction *= -1;
    }
    if (yPos > height - sizeOfTopic/2 || yPos < 0 + sizeOfTopic/2) {
      y_direction *= -1;
    }
  }


//********************************************
//
//              ×× DISPLAY ××
//
//********************************************

  void display() {

    noStroke();
    pushMatrix();
    translate(xPos, yPos);
    scale(zoom);
    fill(shapeTempColor);
    ellipse(0, 0, sizeOfTopic, sizeOfTopic);
    popMatrix();

    if (printTopic) {
      displayTopic();
    }

    if (alreadySeen) {
      pushMatrix();
      translate(xPos, yPos);
      stroke(0, 135);
      rotate(rot);
      line(0, sizeOfTopic/2, 0, -sizeOfTopic/2);

      if (rot < TWO_PI) {
        rot += 0.1;
      }
      else {
        rot = 0;
      }
      popMatrix();
    }
  }

//********************************************
//
//            ×× DISPLAYTOPIC ××
//
//********************************************
  void displayTopic() {
    pushMatrix();

    // in function of my position on the screen
    if (mouseX>0 && mouseX<width/2) {
      translate(x_TopicL, y_TopicL);
    }
    else {
      translate(x_TopicR, y_TopicR);
    }

    fill(topicColor);
    rect(0, 0, w_Topic, h_Topic);
    textSize(12);
    fill(textColor);

    text(content, 0 + w_Topic/10, 0 + h_Topic/2);
    //text(str(espace), 0, 0);
    popMatrix();
  }
}

