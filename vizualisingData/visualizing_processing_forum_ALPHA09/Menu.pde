class Menu {

  //********************************************
  //
  //              ×× DATA ××
  //
  //********************************************

  color [] colorGuide;
  String[] nameOfCategory;
  float x[];
  float y[];

  //********************************************
  //
  //              ×× CONSTRUCTOR ××
  //
  //********************************************

  Menu(color[] _colorGuide, String[] _nameOfCategory) {
    colorGuide = _colorGuide;
    nameOfCategory = _nameOfCategory;
    x = new float[colorGuide.length];
    y = new float[colorGuide.length];
    float val = 20;

    for (int i=0; i<x.length; i++) {
      x[i] = 20;    
      y[i] += val;
      val += 20;
    }
  }

  //********************************************
  //
  //              ×× DISPLAY ××
  //
  //********************************************

  void display() {
    textSize(15);
    for (int i=0; i<colorGuide.length; i++) {
      fill(colorGuide[i]);
      rect(x[i], y[i], 200, 20);
      fill(0);
      float h = textAscent() + textDescent();
      text(nameOfCategory[i], x[i], y[i] + h/1.4);
    }
  }
}

