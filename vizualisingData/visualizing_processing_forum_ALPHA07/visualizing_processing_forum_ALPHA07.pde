
//********************************************
//
//              ×× DATA ××
//
//********************************************

// different rss feed from the Processing forum :
String [] url = {
  "https://forum.processing.org/exhibition/feed", 
  "https://forum.processing.org/programming-questions/feed", 
  "https://forum.processing.org/core-library-questions/feed", 
  "https://forum.processing.org/contributed-library-questions/feed", 
  "https://forum.processing.org/android-processing/feed"
};

String urlForSearching = "https://forum.processing.org/";
String HTMLpage = null;

// String for the new's title
String s_title [][];
// String for the date of each news
String s_date [][];
// String for the url of each news
String s_link[][];
// string for the category of the rss feed
String s_category[][];
String s_author[][];


XMLElement rss[];
XMLElement titleXMLElements [][];
XMLElement pubDateXMLElements [][];
XMLElement linkXMLElements [][];
XMLElement categoryXMLElements[][];
XMLElement authorXMLElements[][];


//  ------  all my different topics/news
News news[][];

PFont myFont;

color newsColor = color(125);
;


int nb [];
int nb_url = 5;

float x_title [][];
float y_title [][];

//********************************************
//
//              ×× SETUP ××
//
//********************************************

void setup() {
  size(800, 800);
  smooth();

  // build my font
  String[] fontList = PFont.list();
  myFont            = createFont(fontList[31], 22);
  textFont(myFont, 15);

  //-----------------------------------
  nb        = new int[nb_url];
  rss       = new XMLElement[nb_url];

  //----------------------------------
  // first I initialize variables
  initializeVariables();
  // after that I fill there.
  fillVariables();
}

//********************************************
//
//        ×× INITIALIZE VARIABLES ××
//
//********************************************

// first I have to initialize my variables

void initializeVariables() {

  for (int k=0; k<nb_url; k++) {

    rss[k] = new XMLElement(this, url[k]);

    // find the number of child per rss to fill the nb[k] variable
    XMLElement count[] = rss[k].getChildren("channel/item/title");
    nb[k]              = count.length;

    for (int i=0; i<nb[k]; i++) {

      titleXMLElements    = new  XMLElement [nb_url][nb[k]];
      pubDateXMLElements  = new  XMLElement [nb_url][nb[k]];
      linkXMLElements     = new  XMLElement [nb_url][nb[k]];
      categoryXMLElements = new  XMLElement [nb_url][nb[k]];
      authorXMLElements   = new  XMLElement [nb_url][nb[k]];

      x_title = new float[nb_url][nb[k]];
      y_title = new float[nb_url][nb[k]];

      s_title    = new String [nb_url][nb[k]];
      s_date     = new String [nb_url][nb[k]];
      s_link     = new String [nb_url][nb[k]];
      s_author   = new String [nb_url][nb[k]];
      s_category = new String [nb_url][nb[k]];

      // And finally, I said that I will create this number 
      // of instances of my class 'News' (nb_url = 5 and 
      // nb[k] = 15, so 5 * 15 = 75 instances/objects)
      news       = new News[nb_url][nb[k]];
    }
  }
}

//********************************************
//
//            ×× FILL VARIABLES ××
//
//********************************************

void fillVariables() {//-------------------
  // fill the variables
  //-------------------
  for (int k=0; k<nb_url; k++) {
    titleXMLElements   [k]  = rss[k].getChildren("channel/item/title");
    pubDateXMLElements [k]  = rss[k].getChildren("channel/item/pubDate");
    linkXMLElements    [k]  = rss[k].getChildren("channel/item/link");
    categoryXMLElements[k]  = rss[k].getChildren("channel/item/category");
    authorXMLElements  [k]  = rss[k].getChildren("channel/item/dc:creator");

    for (int i=0; i<nb[k]; i++) {

      s_title    [k][i] = titleXMLElements[k][i].getContent();
      s_date     [k][i] = pubDateXMLElements[k][i].getContent();
      s_link     [k][i] = linkXMLElements[k][i].getContent();
      s_author   [k][i] = authorXMLElements[k][i].getContent();
      s_category [k][i] = categoryXMLElements[k][i].getContent();

      x_title[k][i] = random(width);
      y_title[k][i] = random(height);

      // build new object from the class News with all this parameters
      news[k][i] = new News(
      x_title   [k][i], //-------- x coordinate
      y_title   [k][i], //-------- y coordinate
      s_title   [k][i], //-------- title of the post
      s_date    [k][i], //-------- date of publication
      s_link    [k][i], //-------- url of the post
      s_author  [k][i], //-------- author of the post
      s_category[k][i], //-------- category of the post
      newsColor         //-------- color of the shape 
      );
    }
  }
}

//********************************************
//
//              ×× DRAW ××
//
//********************************************

void draw() {
  background(255);
  noStroke();

  // same here for 'news' instance, but with double array of instances.
  for (int k=0; k<nb_url; k++) {
    for (int i=0; i<nb[k]; i++) {
      news[k][i].update();
      news[k][i].display();
    }
  }
}

//********************************************
//
//              ×× OVERCIRCLE ××
//
//********************************************

// use this method to detect if you are over an ellipse

boolean overCircle(float x, float y, float diameter) 
{
  float disX = x - mouseX;
  float disY = y - mouseY;
  if (sqrt(sq(disX) + sq(disY)) < diameter/2 ) {
    return true;
  } 
  else {
    return false;
  }
}

